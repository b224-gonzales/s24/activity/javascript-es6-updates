console.log("Hello World!")



// GetCube
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}.`);


// Address Array Destructure

const address = ["Toril", "Davao City", "Phillipines"];

const [district, city, province] = address;

console.log(`I live in ${district} ${city} ${province}.`);


// Destructure Object

const crocodile = {
	specie: "Saltwater Crocodile",
	weight: 1075,
	height: "20 ft 3 in"
};

const { specie, weight, height } = crocodile;

function animal ({specie, weight, measurement}) {
	console.log(`Lolong was a ${specie}. He weighed at ${weight} with a measurement of ${height}.`)
};
animal(crocodile)


// Number Array

const numbers = [1, 2, 3, 4, 5, 15];

numbers.forEach((number) => console.log(number));




// Class Dog


class Dog {
	constructor(name, breed, age) {
		this.name = name;
		this.breed = breed;
		this.age = age;
	};
};

const myDog = new Dog();

myDog.name = "Shamma";
myDog.breed = "Shihtzu";
myDog.age = 5;

console.log(myDog);
